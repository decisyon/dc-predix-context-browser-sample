(function() {
	'use strict';

	window.DcyGenericApi = function() {

		this.completedPath = function(path){
			return resourceURL(path);
		};

		this.encodedLanguageString = function(labelID){
			return getEncodedLanguageString(labelID);
		};

		this.escape = function(str){
			return escape(str);
		};

		this.encodeApex = function(str){
			return encodeApex(str);
		};
	};

}());
