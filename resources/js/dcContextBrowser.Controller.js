(function() {
	'use strict';

	function DCContextBrowserController($scope, $timeout, $q) {

		var target = $scope.DECISYON.target;

		function demoGetChildren(node) {
			var nodeId,
				deferred = $q.defer(),
				children,
				nodeIds = {
					'001-1':{
						'data': [{
								'name': 'A',
								'identifier': '001-1a'
							}, {
								'name': 'B',
								'identifier': '001-1b'
							}, {
								'name': 'C',
								'identifier': '001-1c'
							}, {
								'name': 'D',
								'identifier': '001-1d'
							}, {
								'name': 'E',
								'identifier': '001-1e'
							}, {
								'name': 'F',
								'identifier': '001-1f'
							}, {
								'name': 'G',
								'identifier': '001-1g'
							}, {
								'name': 'H',
								'identifier': '001-1h'
							}, {
								'name': 'I',
								'identifier': '001-1i'
							}, {
								'name': 'AA',
								'identifier': '001-1aa'
							}, {
								'name': 'BB',
								'identifier': '001-1bb'
							}, {
								'name': 'CC',
								'identifier': '001-1cc'
							}, {
								'name': 'DD',
								'identifier': '001-1dd'
							}, {
								'name': 'EE',
								'identifier': '001-1ee'
							}, {
								'name': 'FF',
								'identifier': '001-1ff'
							}, {
								'name': 'GG',
								'identifier': '001-1gg'
							}, {
								'name': 'HH',
								'identifier': '001-1hh'
							}, {
								'name': 'II',
								'identifier': '001-1ii'
							}, {
								'name': 'AAA',
								'identifier': '001-1aaa'
							}, {
								'name': 'BBB',
								'identifier': '001-1bbb'
							}, {
								'name': 'CCC',
								'identifier': '001-1ccc'
							}, {
								'name': 'DDD',
								'identifier': '001-1ddd'
							}, {
								'name': 'EEE',
								'identifier': '001-1eee'
							}, {
								'name': 'FFF',
								'identifier': '001-1fff'
							}, {
								'name': 'GGG',
								'identifier': '001-1ggg'
							}, {
								'name': 'HHH',
								'identifier': '001-1hhh'
							}, {
								'name': 'III',
								'identifier': '001-1iii'
							}
						],
						'meta': {
							'parentId': '001-1'
						} 

					},
					'001-2':{
						'data': [{
								'name': 'Nested Child 1',
								'identifier': '001-2a',
								'isOpenable': true,
								'hasChildren': true
							}, {
								'name': 'Nested Child 2',
								'identifier': '001-2b',
								'isOpenable': true
							}, {
								'name': 'Nested Child 3',
								'identifier': '001-2c',
								'isOpenable': true
							}
						],
						'meta': {
							'parentId': '001-2'
						}
					},
					'001-2a':	{
						'data': [{
							'name': 'Nested Grandchild 1',
							'identifier': '001-2aa',
							'isOpenable': true,
							'hasChildren': true
						}, {
							'name': 'Nested Grandchild 2',
							'identifier': '001-2ab',
							'isOpenable': true
						}, {
							'name': 'Nested Grandchild 3',
							'identifier': '001-2ac',
							'isOpenable': true
						}
						],
						'meta': {
							'parentId': '001-2a'
						}
					},
					'001-2aa':	{
						'data': [{
							'name': 'Nested Great Grandchild 1',
							'identifier': '001-2aba',
							'isOpenable': true
						}, {
							'name': 'Nested Great Grandchild 2',
							'identifier': '001-2abb',
							'isOpenable': true
						}, {
							'name': 'Nested Great Grandchild 3',
							'identifier': '001-2abc',
							'isOpenable': true
						}],
						'meta': {
							'parentId': '001-2aa'
						}
					}									
				};//hardcoded with the json for now.Later can be changed to a service call based on requirement.
			//when the context makes the first call onload that time we will not have any node selected
			if(!node) {
					children = {
							'data': [
								{
									'identifier': '001-1',
									'name': 'Lots of Children',
									'isOpenable' : true,
									'hasChildren': true
								},
								{
									'identifier': '001-2',
									'name': 'Deep nested',
									'isOpenable' : true,
									'children': [
										{
											'identifier': '001-2a',
											'name': 'Nested Child 1',
											'isOpenable' : true,
											'children': [
												{
												'identifier': '001-2aa',
												'name': 'Nested Grandchild 1',
												'isOpenable' : true,
												'children' : [{
												'identifier': '001-2aba',
												'name': 'Nested Great Grandchild 1',
												'isOpenable' : true
												},
												{
												'identifier': '001-2abb',
												'name': 'Nested Great Grandchild 2',
												'isOpenable' : true
												},
												{
												'identifier': '001-2abc',
												'name': 'Nested Great Grandchild 3',
												'isOpenable' : true,
												'selectedAsset': true
												}],
												'meta': { 'parentId': '001-2ab' }
												},
												{
												'identifier': '001-2ab',
												'name': 'Nested Grandchild 2',
												'isOpenable' : true
												},
												{
												'identifier': '001-2ac',
												'name': 'Nested Grandchild 3',
												'isOpenable' : true
												}
											],
											'meta': { 'parentId': '001-2a' }
										},
										{
											'identifier': '001-2b',
											'name': 'Nested Child 2',
											'isOpenable' : true
										},
										{
											'identifier': '001-2c',
											'name': 'Nested Child 3',
											'isOpenable' : true
										}
									],
								'meta': { 'parentId': '001-2' }
								},
								{
									'identifier': '001-3',
									'name': 'Nothing Below Me',
									'isOpenable' : true
								},
								{
									'identifier': '001-4',
									'name': 'Nothing Below, Not openable'
								}
							],
							'meta': { 'parentId': null } 

						}; // This is the first data to context browser shouldn't be removed else context will not work.
					} else {
					    // when a child is clicked in the context browser if the node is there in the nodeIds else it will return the else part
					    nodeId = node.identifier;
            			if (nodeIds[nodeId]) {
            				children = nodeIds[nodeId];
            				deferred.resolve(children);
            			}
            			else {
            				deferred.resolve({data:	[],
            				meta:{parentId: nodeId}});
            			}
					}
			deferred.resolve(children);
			//don't forget to return the promise!
			return deferred.promise;
		}


		var registerDataConnector = function() {
			$scope.DECISYON.target.registerDataConnector(function(requestor) {
				var contextRequestData = [],
				    defered = $q.defer();
				var getData = function(node) {
				        //function to get the data when data request is made by context browser
					    demoGetChildren(node).then(function(data) {
							contextRequestData[0] = data;
						});
					    defered.resolve(contextRequestData);
					    return defered.promise;
				};

			    return { data: getData };
            });
		};


		/* Initialize */
		var inizialize = function() {
			registerDataConnector();
		};


		inizialize();

	}
	DCContextBrowserController.$inject = ['$scope','$timeout','$q'];
	DECISYON.ng.register.controller('dcContextBrowserCtrl', DCContextBrowserController);

}());