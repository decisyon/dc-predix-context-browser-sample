/**
 * Ritorna true se l'oggetto passato non è undefined e non è null.
 * 
 * !!! Può essere utilizzato solo su variabili dichiarate o passate per parametro alla funzione. Altrimenti
 *     è necessario fare il controllo puntuale. 
 * 
 * @param obj
 * @returns {Boolean}
 */
function isDefined(obj) {
	return (typeof obj !== "undefined") && (obj != null);
}

// utilizzato come helper per generare il codice necessario da inserire in un tag <a> per gestire il link ad un utente
function anchorUser(userId) {
	return "href='#' onclick='showUserTip(this, "+userId+");return false;'";
}

function splitString(text, split) {
	if (isDefined(text)) {
		return text.split(split);
	}
	else {
		return ["","",""];
	}
}

/**
 * Effettua la chiamata al dataprovider specificato richiamando la funzione di callback richiesta
 * con il contenuto già decorato tramite template.
 * 
 * @author Costantini Simone
 * @param uid
 * @param context
 * @param callback
 */
function renderProviderDataForContextTo(context, objectSelector, controllerUid){
//	var jsons = {};
	// Controllo se il json è in formato stringa
//	if (context != null && typeof context == "string" && context!="") {
//		jsons = JSON.parse(context);
//	}
	//$(objectSelector).data("ctx", jsons);
	//$(objectSelector).attr("tmplNode", "mineTMPL");
	
	requestProviderDataForContext(context, objectSelector, controllerUid, decorateContentToSelector);
}

/**
 * Effettua la chiamata al dataprovider specificato richiamando la funzione di callback richiesta
 * modificando la view con quella specifica selezionata.
 * @author fferraiu
 * @param ctx
 * @param objectSelector
 * @param controllerUid
 * @param specificView
 */
function renderProviderDataForCtxByView(ctx, objectSelector, controllerUid, specificView, specificDecorateFunction, errorDecorateFunction){
	if (specificView)
		addDataToCtx(ctx, "viewTMPL", specificView);
	
	var decorateFunction = specificDecorateFunction != null ? specificDecorateFunction : decorateContentToSelector;
	requestProviderDataForContext(getRasterCtxContainer(ctx), objectSelector, controllerUid, decorateFunction, errorDecorateFunction);
}

/**
 * Effettua la chiamata al dataprovider specificato richiamando la funzione di callback richiesta,
 * consentendo di specificare un contesto per la stessa
 * 
 * @author Costantini Simone
 * @param contextIn
 * @param selector
 * @param controllerUid
 * @param callback
 * @param errorCallback
 */
function requestProviderDataForContext(contextIn, selector, controllerUid, callback, errorCallback){
	$.ajax({
		data: {revent:'GET_DATA',a0:controllerUid,a1:contextIn},
		success: function(response, textStatus, xhr) {
			callback(response, selector, controllerUid, contextIn);
		},
		error: function(response, textStatus, xhr) {
			defaultErrorAjaxRequest(response, textStatus, xhr, this);
			logToConsole('requestProviderDataForContext AJAX call Error:' + response.responseText);
		},
		dpParams:{
				contextIn : contextIn,
				selector : selector,
				controllerUid : controllerUid,				
				errorCallback : errorCallback					
		}
	});
}

/**
 * Effettua la chiamata al dataprovider specificato richiamando la funzione di callback richiesta
 * 
 * @author Costantini Simone
 * @param uid
 * @param context
 * @param callback
 */
function requestProviderData(controllerUid, callback){  
	return requestProviderDataForContext(null, null, controllerUid, callback);
}

/**
 * Effettua la chiamata al dataprovider specificato richiamando la funzione di callback richiesta
 * con il contenuto già decorato tramite template.
 * 
 * @author Costantini Simone
 * @param uid
 * @param context
 * @param callback
 */
function renderProviderDataTo(objectSelector, controllerUid){
	return renderProviderDataForContextTo(null, objectSelector, controllerUid);
}

/**
 * Importa uno script a partire dal path (fa riferimento al server definito nella proprietà DecisyonWebUrl)
 */
function SCRIPT(arg1, owner) {
	if (isDefined(owner)) {
		var path = RESOURCE(owner, arg1);
		
		// add the returned content to a newly created script tag
		if (!scriptExist(path) && !scriptExist(path+"&noShowError=true")) {
			var content = "";
			$.ajax({
				async: false,
				type: "GET",
			    dataType : "text",
				url: path+"&noShowError=true",
				success: function(response, textStatus, xhr) {
					content = response;
					// add the returned content to a newly created script tag
					var se = document.createElement('script');
					se.type = "text/javascript";
					se.text = content;
					se.setAttribute("path", path);
					document.getElementsByTagName('head')[0].appendChild(se);
				},
				error: function(response, textStatus, xhr) {
					logToConsole(response.responseText);
					requestTemplateModels(url, specificViewToLoad, callback);
				}
			});
		}
	}
	else {
		if (arg1.charAt(0) == "/") {
			var path = getEnvVar('jsToken') + arg1;
		}
		else {
			var path = getEnvVar('jsToken') + "/" + arg1;
		}
		
		try {
			if (!scriptExist(path) && !scriptExist(path+"?noShowError=true")) {
				var content = "";
				$.ajax({
					async: false,
					type: "GET",
				    dataType : "text",
					url: resourceURL(path+"?noShowError=true"),
					success: function(response, textStatus, xhr) {
						content = response;
						// add the returned content to a newly created script tag
						var se = document.createElement('script');
						se.type = "text/javascript";
						se.text = content;
						se.setAttribute("path", path);
						document.getElementsByTagName('head')[0].appendChild(se);
					},
					error: function(response, textStatus, xhr) {
						logToConsole(response.responseText);
						requestTemplateModels(url, specificViewToLoad, callback);
					}
				});
			}
		}
		catch(e){
			logToConsole(e);
		}
	}
}

/**
 * Importa un css a partire dal path (fa riferimento al server definito nella proprietà DecisyonWebUrl)
 */
function CSS(path, owner) {
	if (isDefined(owner)) {
		var path = RESOURCE(owner, path);
		
		if (!cssExist(path) && !cssExist(path+"&noShowError=true")) {
			$.ajax({
				async: false,
				type: "GET",
			    dataType : "text",
				url: path+"&noShowError=true",
				success: function(response, textStatus, xhr) {
					var content = response;
					// add the returned content to a newly created script tag
					var cssNode = document.createElement('link');
					cssNode.type = 'text/css';
			
					cssNode.rel = 'stylesheet';
					cssNode.href = path+"&noShowError=true";
					document.getElementsByTagName('head')[0].appendChild(cssNode);
					if ($.browser.msie) {
						if (parseInt($.browser.version) <=9 && fix_ie_css_limit)
							fix_ie_css_limit();
					}
				},
				error: function(response, textStatus, xhr) {
					logToConsole(response.responseText);
					requestTemplateModels(url, specificViewToLoad, callback);
				}
			});
		}
	}
	else {
		if (path.charAt(0) == "/") {
			var path = getEnvVar('jsToken') + path;
		}
		else {
			var path = getEnvVar('jsToken') + "/" + path;
		}
		
		if (!cssExist(path) && !cssExist(path+"?noShowError=true")) {
			// adding the script tag to the head as suggested before
			var cssNode = document.createElement('link');
			cssNode.type = 'text/css';
	
			cssNode.rel = 'stylesheet';
			cssNode.href = resourceURL(path+"?noShowError=true");
			document.getElementsByTagName('head')[0].appendChild(cssNode);
			//$("head").append(cssNode);
			
			if ($.browser.msie) {
				if (parseInt($.browser.version) <=9 && fix_ie_css_limit)
					fix_ie_css_limit();
			}
		}
	}
}

function RESOURCE(owner, key) {
	return resourceURL("cassiopeaWeb/misc/imageRepoSrc.jsp?key=" + key + "&owner=" + owner);
}

/**
 * Restituisce un URL a partire dal path (fa riferimento al server definito nella proprietà DecisyonWebUrl)
 */
function resourceURL(path, token) {
	token = token ? token : '';
	var pre = ENV_VARS.getValue("contextPath");
	
	if (typeof path == "undefined" || path == null) {
		return pre;
	}

	if (pre.charAt(pre.length-1) == "/") {
		pre = pre.substring(0,pre.length-1);		
	}

	var post = path;
	if (post.charAt(0) == "/") {
		post = post.substring(1);
	}

	return pre + ((token.charAt(token.length-1) == '/') ? token : token + '/') + post;
}

//Controlla se lo script è già stato importato
function scriptExist(path) {
	var result = false;
	
	if (!isDefined(path)) {
		return false;
	}
	
	$("script").each(function() {
	    var src = $(this).attr("src");
	    var attrPath = $(this).attr("path");
	    
	    if(isDefined(src) && src.substr(-path.length) == path ) {
	    	result =  true;
	    	
	    	// Ritornando false dalla funzione di callback dell'each si interrompe il ciclo
	    	return false;
	    }
	    
	    if (isDefined(attrPath) && attrPath.substr(-path.length) == path ) {
	    	result =  true;
	    	
	    	// Ritornando false dalla funzione di callback dell'each si interrompe il ciclo
	    	return false;
	    }
	});
	
	return result;
}

//Controlla se il css è già stato importato
function cssExist(path) {
	var result = false;
	
	if (!isDefined(path)) {
		return false;
	}
	
	$("link").each(function() {
	    var href = $(this).attr("href");
	    if(isDefined(href) && href.indexOf(path)!=-1) { // l'istruzione (href.substr(-path.length) == path) è stata sostituita perché non funziona su ie8
	    	result =  true;
	    	
	    	// Ritornando false dalla funzione di callback dell'each si interrompe il ciclo
	    	return false;
	    }
	});
	
	return result;
}

/**
 * Effettua la renderizzazione di un json e ne sostituisce il contenuto agli elementi target afferenti al selettore specificato
 * 
 * @author Costantini Simone
 * @param uid
 * @param context
 * @param callback
 */
function decorateContentToSelector(jsonContent, selector, controllerUid, contextIn){
	try {
		//gestisce che sia un Json valido sia in caso di response che invocazione con oggetto
		var jsons = getValidJsonContent(jsonContent);
		var localVars = {"ctx" : jsons.ctx};
		
		var contentUID = jsons.contentUID;
		var useDataLink = jsons.useDataLink;
		var engineV = jsons.engineV;
		
		var specificCssClassName = (jsons.specificStyle)?jsons.specificStyle.specificCssClassName : null;
		
		if (jsons.specificStyle){			
			setSpecificCSS(jsons.specificStyle.specificCssUrl);
		}
		
		if (contextIn) {
			var ctxIn = getValidJsonContent(contextIn).ctx;
			removeDataFromCtx('registerContent', ctxIn); // questo parametro non va inoltrato alle chiamate successive.;
			contextIn = getRasterCtxContainer(ctxIn);
		}
		var nodeInfo = {
				selector: selector,
				mainInfo: jsons, 
				useDataLink: useDataLink,
				winRef : window,
				ctxIn : contextIn,
				engineV : engineV
		};
		
		if (contentUID){
			rootPage.CONTENT_REFERENCE.addItem(contentUID, nodeInfo);
		}
		var isChanged = false;
		$(selector).each(function() {
			var isRendered = ($(this).find('dcyContent:first').size() > 0) ? true : false;
			var parentNode = appendDecoratedElementToCustomNode($(this), specificCssClassName, jsons, controllerUid, ctxIn);
							 
			if (contentUID && useDataLink){
				if (!isRendered)
					renderDataLinkTemplate(jsons, parentNode, contentUID, localVars);
				else
					isChanged = true;
			}else{
				renderizerTemplate(jsons, parentNode, localVars);
			}
		});
		if (isChanged){
			rootPage.raiseObservable(jsonContent, selector, controllerUid, contextIn, contentUID);
			isChanged = false;
		}
	
	}catch(err){
		logToConsole('decorateContentToSelector: '+err.message);
	}
};

/*
 * Renders template with datalink
 */
function renderDataLinkTemplate(jsons, parentNode, contentUID, localVars) {
	eval('rootPage.$.link.'+jsons.view+'(parentNode, rootPage.CONTENT_REFERENCE.getValue(contentUID).mainInfo.data,'+JSON.stringify(localVars)+')');
	var data = rootPage.CONTENT_REFERENCE.getValue(contentUID).mainInfo.data;
	$(rootPage.CONTENT_REFERENCE.getValue(contentUID).winRef).on("beforeunload", function() {
		rootPage.$.unlink[jsons.view](parentNode, data, JSON.stringify(localVars));
	});
	evaluateJavascript(parentNode);
}

/**
 * Restituisce il json, eseguendo il parse se è in formato stringa
 * @param jsonContent
 * @returns
 */
function getValidJsonContent(jsonContent){
	var jsons = jsonContent;
	if (jsonContent != null && typeof jsonContent == "string" && jsonContent != '') {
		jsons = JSON.parse(jsonContent);
	}
	return jsons;
};

/**
 * Sostituisce il contenuto di un elemento iframe
 * @param $iframe
 * @param result
 */
function decorateContentToIframe($iframe, content, json){
	var doc = $iframe.document;
    if($iframe.contentDocument) {
        doc = $iframe.contentDocument; // For NS6
    }
    else if(iframe.contentWindow) {
        doc = $iframe.contentWindow.document; // For IE5.5 and IE6
    }

    doc.open();
    doc.writeln(content);
    doc.close(); 
};
/**
 * Importa il css specificato solo se non risulta già importato
 */
function setSpecificCSS(specificCssUrl){
	if (specificCssUrl!=null)
		cssLoader(specificCssUrl);
};

/**
 * Imposta il contenuto del selettore
 */
function decorateElementContent(element, data, json){
	if (element.tagName == "IFRAME") {
		decorateContentToIframe(element, data, json);
	}else {				
		var comment = getCommentCode(json) ;
		var ua = $.browser;
		if ( ua.msie && ua.version.slice(0,1) == "8" ) {
			element.innerHTML = comment  + '<input type="hidden" \>' + data;
		} 
		else {
			element.innerHTML = comment + data;
		}
	}			
};

/** Commento per i dati
 * @author Costantini Simone
 * @param json
 * @returns {String}
 */
function getCommentCode(json){
	try {
		var comment = "";
		if(ENV_VARS.isInDevelopmentOrTesting()){
			var jsonString = JSON.stringify(json, null, 4);
			jsonString = replaceAll(jsonString, "<", "&gt;");
			jsonString = replaceAll(jsonString, ">", "&lt;");
			comment = '<!-- DEBUG RENDERED DATA:' + jsonString + '-->';
		}
		return comment;
	} catch (e) {
		return "";
	}
}
/**
 * Create a custom tag with a specific class (es. jquery theme)
 * and then insert the element into the custom tag
 */
function appendDecoratedElementToCustomNode(element, specificCssClassName, jsons, controllerUid, contextIn){
	var parentNode = getElementIntoWhichTheCustomTagIsInsered(element);
	var parentTemplate = null;
	parentTemplate = $('<dcyContent></dcyContent>');
	
    if (specificCssClassName != null){
    	parentTemplate.addClass(specificCssClassName);
    }
    
	var nodeInfo = {
			ctx: jsons.ctx,
			ctxIn: getValidJsonContent(contextIn), 
			controllerUid : controllerUid
	};
    
	parentTemplate.data("nodeInfo", nodeInfo);
	
	parentNode.empty();
	parentNode.append(parentTemplate);
		
	return parentTemplate.get(0);
};


/**
 * Recupera il ctx dal nodo e ne preleva il value.
 * If multivalue return the list of values.
 * 
 * @param id
 * @param node
 * @returns
 */
function getValueFromCtx(id, node) {
	var ctx = getCtx(node);
	return getCtxValue(id, ctx);
};

/**
 * Recupera il nodo più vicino (closest) che contiene nel proprio data il ctx
 * 
 * @param node
 * @returns
 */
function getCtxNode(node) {
	if ($(node) && $(node).prop("tagName") && $(node).prop("tagName").toLowerCase() == 'dcycontent') {
		return  $(node);
	}
	
	var $tmplNode = $(node).closest('dcyContent');
	return $tmplNode;
}

/**
 * Recupera il ctx dal nodo
 * 
 * @param node
 * @returns
 */
function getCtx(node) {
	return getNodeInfoFromNode(node).ctx;
}

/**
 * Recupera il ctxIn dal nodo e ne restiruisce uno clonato, per non compromettere l'originale.
 * 
 * @param node
 * @param noClone permette di ottenere il ctxIn originale.
 * @returns
 */
function getCtxIn(node, noClone) {
	var ctxIn = getNodeInfoFromNode(node).ctxIn;
	if (noClone) {
		return ctxIn; 
	}
	else {
		return cloneCtxObject(ctxIn);
	}
}

/**
 * Recupera il controllerUID dal nodo
 * 
 * @param node
 * @returns
 */
function getControllerUID(node) {
	return getNodeInfoFromNode(node).controllerUid;
}

function getNodeInfoFromNode(node) {
	var $tmplNode = getCtxNode(node);
	var nodeInfo = $tmplNode.data('nodeInfo');
	if (!nodeInfo) {
		nodeInfo = {
			ctx: [],
			ctxIn: []
		};
		$tmplNode.data("nodeInfo", nodeInfo);
	}
	return nodeInfo;
}

function setCtx(node, ctx) {
	var $tmplNode = getCtxNode(node);
	var nodeInfo = getNodeInfoFromNode(node);
	nodeInfo.ctx = ctx;
	$tmplNode.data('nodeInfo', nodeInfo);
}

function setCtxNodeValue(node, id, value, defaultValue) {
	var ctx = getCtx(node);
	if (ctx) {
		for (var i=0; i<ctx.length; i++) {
			if (ctx[i].id == id) {
				ctx[i].value = value;
				return ctx;
			}
		}
		var contextData = {};
		contextData.id = id;
		contextData.value = value;
		if (defaultValue)
			contextData.defaultValue = defaultValue;
		ctx.push(contextData);
		return ctx;
	}
}

/**
 * Utilizzato solo per ottenere il ctx come helper
 * 
 * @param jsRendView view hierarchy del jsRenderer
 * @param ctxObject optionals, if invoked as helper
 * @returns
 */
function discoverCtxHelper(jsRendView, ctxObject) {
	if (!isDefined(ctxObject)) {
		if (isDefined(jsRendView) && isDefined(jsRendView.ctx)) {
			ctxObject = jsRendView.ctx.ctx;
		}
		else {
			throw new Error("discoverCtxHelper(): missing ctxObject");
		}
	}
	
	return ctxObject;
}

/**
 * Get context value from id. If multivalue return the list of values.
 * 
 * @param id
 * @param ctxObject optionals, if invoked as helper
 * @param jsRendView view hierarchy del jsRenderer
 * @returns
 */
function getCtxValue(id, ctxObject, jsRendView) {
	if (!isDefined(jsRendView)) {
		jsRendView = this;
	}
	var contextData = getContextData(id, ctxObject, jsRendView);
	if (isDefined(contextData)) {
		if (contextData.value) {
			return contextData.value;
		}
		else if(isDefined(contextData.values)){
			return contextData.values;
		}
	}
	
	return "";
};

/**
 * Get context the boolean value from id.
 * 
 * @param id
 * @param ctxObject optionals, if invoked as helper
 * @param jsRendView view hierarchy del jsRenderer
 * @returns
 */
function getCtxValueBool(id, ctxObject, jsRendView) {
	if (!isDefined(jsRendView)) {
		jsRendView = this;
	}
	var value = getCtxValue(id, ctxObject, jsRendView);
	return value.toLowerCase() == 'true' || value == '1';
}

/**
 * Ritorna il context container in formato raster (serializzato in json).
 * 
 * @param ctxObject
 * @param auxCtx0 (optional)
 * @param auxCtx1 (optional)
 * @param auxCtx2 (optional)
 * @returns
 */
function getRasterCtxContainer(ctxObject, auxCtx0, auxCtx1, auxCtx2) {
	if (!isDefined(ctxObject)) {
		throw new Error("getRasterCtxContainer(): missing ctxObject");
	}
	var contextContainer = {}; 
	contextContainer.ctx = ctxObject;
	contextContainer.auxCtx0 = auxCtx0;
	contextContainer.auxCtx1 = auxCtx1;
	contextContainer.auxCtx2 = auxCtx2;
	var rasterCtx = JSON.stringify(contextContainer);

	return rasterCtx;
}

/**
 * Ritorna un nuovo array di contesti pulito.
 * 
 * @returns {Array}
 */
function buildNewCtxObject() {
	var ctx = [];
	return ctx;
}

/**
 * Costruisce un cxtObject a partire dal serializzato di un form 
 * 
 * @param ser
 */
function buildNewCtxFromSerializedForm(serForm) {
	var ctxObject = buildNewCtxObject();
	for (var i=0; i<serForm.length; i++) {
		var field = serForm[i];
		addDataToCtx(ctxObject, field.name, field.value);
	}
	
	return ctxObject;
};

/**
 * Clona e restituisce un intero ctxObject 
 * 
 * @param ctxObjectSrc
 * @returns
 */
function cloneCtxObject(ctxObjectSrc) {
	if (!isDefined(ctxObjectSrc)) {
		throw new Error("cloneCtxObject(): missing ctxObjectSrc");
	}
	var ctxObject = buildNewCtxObject();
	for (var i=0; i<ctxObjectSrc.length; i++) {
		addContextDataToCtx(ctxObject, cloneContextData(ctxObjectSrc[i]));
	}
	
	return ctxObject;
}


/**
 * Costruisce un contextData a partire da id e valori
 * 
 * @param id
 * @param value necessario, può essere anche un array (multivalue)
 * @param defaultValue opzionale, può essere anche un array (multivalue) 
 * @returns 
 */
function buildContextData(id, value, defaultValue) {
	if (!isDefined(id) && !isDefined(value)) {
		throw new Error("buildContextData(): missing id o value");
	}
	var contextData = {};
	contextData.id = id;
	if (!Array.isArray(value)) {
		contextData.value = value;
	}
	else {
		contextData.values = value;
	}
	if (isDefined(defaultValue)) {
		if (!Array.isArray(defaultValue)) {
			contextData.defaultValue = value;
		}
		else {
			contextData.defaultValues = value;
		}
	}
	
	return contextData;
}

/**
 * Clona un contextData
 * 
 * @param contextData
 * @returns 
 */
function cloneContextData(contextData) {
	if (!isDefined(contextData)) {
		throw new Error("cloneContextData(): missing contextData");
	}
	
	var clonedData = {};
	clonedData.id = contextData.id;
	clonedData.value = contextData.value;
	clonedData.defaultValue = contextData.defaultValue;
	clonedData.values = contextData.values;
	clonedData.defaultValues = contextData.defaultValues;

	return clonedData;
}

/**
 * Add a context data to context specifying id, value or multi values array and defaultValue or default multi values array (optionals)
 * 
 * @param ctxObject
 * @param id
 * @param value necessario, può essere anche un array (multivalue)
 * @param defaultValue opzionale, può essere anche un array (multivalue) 
 * @returns
 */
function addDataToCtx(ctxObject, id, value, defaultValue) {
	if (!isDefined(ctxObject)) {
		throw new Error("addDataToCtx(): missing ctxObject");
	}
	addContextDataToCtx(ctxObject, buildContextData(id, value, defaultValue));
};

/**
 * Aggiunge un context data al ctxObject
 * 
 * @param ctxObject
 * @param contextData
 */
function addContextDataToCtx(ctxObject, contextData) {
	if (!isDefined(ctxObject) || !isDefined(contextData)) {
		throw new Error("addDataToCtx(): missing ctxObject or contextData");
	}
	var idx = getCtxIndexOf(contextData.id, ctxObject);
	if (idx != -1) {
		ctxObject[idx] = contextData;
	}
	else {
		ctxObject.push(contextData);
	}
}

/**
 * Restiusce l'indice del data nel ctxObject
 * 
 * @param id identificatore del data da trovare.
 * @param ctxObject array di context data. optionals, if invoked as helper
 * @param jsRendView view hierarchy del jsRenderer
 * @returns {Number} l'indice del data cercato, -1 se non trovato.
 */
function getCtxIndexOf(id, ctxObject, jsRendView) {
	if (!isDefined(jsRendView)) {
		jsRendView = this;
	}
	if (!isDefined(id)) {
		throw new Error("getCtxIndexOf(): missing id");
	}
	ctxObject = discoverCtxHelper(jsRendView, ctxObject);
	for (var i=0; i<ctxObject.length; i++) {
		if (ctxObject[i].id == id) {
			return i;
		}
	}
	return -1;
}

/**
 * Restituisce il context data con un certo id contenuto nel ctxObject
 * 
 * @param id
 * @param ctxObject optionals, if invoked as helper
 * @param jsRendView view hierarchy del jsRenderer
 * @returns
 */
function getContextData(id, ctxObject, jsRendView) {
	if (!isDefined(jsRendView)) {
		jsRendView = this;
	}
	ctxObject = discoverCtxHelper(jsRendView, ctxObject);
	var idx = getCtxIndexOf(id, ctxObject, jsRendView);
	if (idx != -1) {
		return ctxObject[idx];
	}
	
	return null;
}

/**
 * Fa la fusione dei due ctxObject dando precedenza ai data che stanno in ctxObject1.
 * I due ctxObject non vengono alterati.
 * 
 * @param ctxObject1
 * @param ctxObject2
 */
function mergeContextData(ctxObject1, ctxObject2) {
	if (!isDefined(ctxObject1) || !isDefined(ctxObject2)) {
		throw new Error("addDataToCtx(): missing ctxObject1 or ctxObject2");
	}
	var ctxObject = buildNewCtxObject();
	for (var i=0; i<ctxObject2.length; i++) {
		addContextDataToCtx(ctxObject, cloneContextData(ctxObject2[i]));
	}
	for (var i=0; i<ctxObject1.length; i++) {
		addContextDataToCtx(ctxObject, cloneContextData(ctxObject1[i]));
	}
	return ctxObject;
}

/**
 * Copia un dato di contesto con identificatore id, da ctxObjSrc a ctxObjDest 
 * 
 * @param id
 * @param ctxObjSrc
 * @param ctxObjDest
 */
function copyContextData(id, ctxObjSrc, ctxObjDest) {
	if (!isDefined(ctxObjSrc) || !isDefined(ctxObjDest)) {
		throw new Error("copyContextData(): missing ctxObjSrc or ctxObjDest");
	}
	
	var contextData = getContextData(id, ctxObjSrc);
	if (isDefined(contextData)) {
		addContextDataToCtx(ctxObjDest, cloneContextData(contextData));
	}
}

/**
 * Rimuove dal context un valore in base all'id.
 * 
 * @param id
 * @param ctxObject
 * @param jsRendView view hierarchy del jsRenderer
 */
function removeDataFromCtx(id, ctxObject, jsRendView) {
	if (!isDefined(jsRendView)) {
		jsRendView = this;
	}
	ctxObject = discoverCtxHelper(jsRendView, ctxObject);
	var idx = getCtxIndexOf(id, ctxObject, jsRendView);
	if (idx != -1) {
		ctxObject.splice(idx, 1);
	}
}

 function replaceApexWithDouble(str) {
	 return str.replace(/'/g,"\\\"");
 }
 
 function encodeApex(str) {
	 if (str)
		 return str.replace(/'/g,"&#39;");
	 return "";
 }
 
function escapeSingleQuotes(str) {
	if (str)
		 return str.replace(/'/gi,"\\'");
	return "";
}
 
 /**
  * Gestisce il Context
  * @author Tantalo Christian Alessandro
  */
 function ctxManager(ctx) {
	  this.ctxObject = new Array();
	  
	  this.initByCtx = function(ctx) {
		  if (ctx) {
			  if (typeof ctx=="string") {
				  this.ctxObject = JSON.parse(ctx);
				  if (this.ctxObject.ctx)
					  this.ctxObject = this.ctxObject.ctx; 
			  }
			  else
				  this.ctxObject = ctx;
		  }
		  return this;
	  };

	  this.delegateForAjax = function(savingCtx, successCtx){
	      for(var i in this.ctxObject)
	    	  savingCtx[i] = this.ctxObject[i];
		  return this.ctxObject;		
	  };
	  this.set = function(id, value, defaultValue) {
		  	for(var i in this.ctxObject) {
		  		if(this.ctxObject[i].id == id) {
		  			this.ctxObject[i].value = value;
		  			return this;
		  		}
		  	}
			var contextData = {};
			contextData.id = id;
			contextData.value = value;
			if (defaultValue)
				contextData.defaultValue = defaultValue;
			this.ctxObject.push(contextData);
			return this;
	  };
	  this.toString = function() {
		  return getRasterCtxContainer(this.ctxObject);
	  };
	  
	  this.toObject = function() {
		  return this.ctxObject;
	  };
	  
	  if (ctx!=null)
		  this.initByCtx(ctx);
 }


  function getParamFunctionByType(param) {
	  return typeof param=="string"?"'" + param + "'":param;
  }

  /**
   * Ritorna il codice in formato stringa per l'apertura di un tooltip.
   */	  
  function getTooltipJSCode(dataProviderId, ctx){
	  var fun = "createJQTooltipByDataProvider(";
	  fun += "$(this), ";
	  fun += getParamFunctionByType(dataProviderId) + ", ";
	  fun += "'" + ctx.replace(/\"/g,"\\'") + "'";
	  fun += ");";
	  
	  return fun;
  }

  function getDshPageUrl(dshId, owner, qsParams) {
	  return pathToRoot + 'cassiopeaWeb/dsh/dshPage.jsp.cas?revent=LW_DSH_LINK&dshID=' + dshId + '&csMLO=' + owner + qsParams;
  }
  
  function navigateToContentCode(scope, owner, contentType, contentId, a1, a2, a3, a4, a5) {
	  a1 = (a1 == null) ? "" : ", "+a1;
	  a2 = (a2 == null) ? "" : ", '"+a2+"'";
	  a3 = (a3 == null) ? "" : ", "+JSON.stringify(a3);
	  contentType = "" + contentType;
	  switch(contentType) {
	  
	  // REPORT
	  case "1": 
		  if (scope == 'WIDGET' || scope == 'TAG') {
		      // il metodo manageContentForwardWidgetReport � posizionale: a1 va mantenuto.
			  a1 = (a1 == "") ? ", ''" : a1;
			  return "manageContentForwardWidgetReport('"+owner+"', "+contentId+a1+a2+")";
		  }
		  else
			  return "manageContentForwardReport('"+owner+"', '"+contentId+"')";
		  break;
	  
      // MASHBOARD
	  case "121":		  
		  if (scope == 'WELCOME'){
			  //csMLO = owner;
			  return "openMashboardFromWelcome('"+owner+"', '"+contentId+"', '"+$.trim(replaceAll(a1,",",""))+"');return false;";
		  }
		  else if (scope == 'SPACES'){		  
			  return "openSpaceFromWelcome('"+owner+"', '"+contentId+"', '"+$.trim(replaceAll(a1,",",""))+"');return false;";
		  }
		  else if (scope == 'WIDGET' || scope == 'TAG') {
		      // il metodo manageContentForwardWidget � posizionale: a1 va mantenuto.
			  a1 = (a1 == "") ? ", ''" : a1;
			  return "manageContentForwardWidget('"+scope+"', '"+owner+"', "+contentId+", null, null, null, null, null"+a1+a2+")";
	      }
		  else{		  
			  return "manageContentForward('"+owner+"', "+contentId+")";
		  }
		  break;
		  
      // TASK
	  case "372":
		  if (scope == 'WIDGET' || scope == 'TLIST' || scope == 'WELCOME'){
			  return "manageContentForwardTask('"+owner+"', '"+contentId+"', '"+scope+"')";
		  } else {
			  return "manageContentForwardTask('"+owner+"', '"+contentId+"')";
		  }
		  break;
		  
	  // SOCIAL_DOCUMENT
	  case "1004": 
		  if (scope == 'WIDGET' || scope == 'TAG' || scope == 'STREAM'){
		      // il metodo manageContentForwardWidget � posizionale: a1 va mantenuto.
			  a1 = (a1 == "") ? ", ''" : a1;
			  return "manageContentForwardWidget('"+scope+"', 'MASTERM', '699354622180595', '"+contentId+"', 1004 "+a1+a2+a3+")";
		  } else {
			  return "manageContentForward('MASTERM', '699354622180595', '"+contentId+"', 1004 "+a1+a2+a3+")";
		  }
		  break;
		  
	  // SOCIAL_BLOG
	  case "1006": 
	  	  if (scope == 'WIDGET' || scope == 'TAG' || scope == 'STREAM'){
		      // il metodo manageContentForwardWidget � posizionale: a1 va mantenuto.
			  a1 = (a1 == "") ? ", ''" : a1;
		  	  return "manageContentForwardWidget('"+scope+"', 'MASTERM', '699354622180595', '"+contentId+"', 1006 "+a1+a2+a3+")";
		   } else {
			  return "manageContentForward('MASTERM', '699354622180595', '"+contentId+"', 1006 "+a1+a2+a3+")";
		  }
		  break;
		  
	  // SOCIAL_SIGNAL
	  case "1007":
		  if (isDefined(contentId)) { 
			if (scope == 'WIDGET' || scope == 'CLIST' || scope == 'WELCOME'){
				return "manageContentForwardSignal('"+owner+"', '"+contentId+"', '"+scope+"')";
			} else {
				return "manageContentForwardSignal('"+owner+"', '"+contentId+"')";
			}
		  }
		  //return "manageContentForward('"+owner+"', '699354622180595', '"+contentId+"', 1007 "+a1+a2+a3+")";
		  break;
		  
	  // SOCIAL_CONVERSATION
	  case "1008":
		  return "manageContentForward('"+owner+"', '699354622180595', '"+contentId+"', 1008 "+a1+a2+a3+")";
		  break;
		  
	  // USER
	  case "500":
		  if (scope == 'TAG'){
//			  return "var opt=getJQTipDefaultOpts(false, false, null);opt.show.solo=true;createJQTooltipByDPWithTitleAndOpts($(this), null, 'userInfo', new ctxManager().set('userId', '"+contentId+"').toString(), '', opt)";
			  return "showUserTip($(this), "+contentId+");stopPropagation(event);return false;";
		  }
		  else {
			  return "rootPage.userProfile('"+owner+"', '"+contentId+"')";
		  }
		  break;
		  
	  // SOCIAL_AREA_GROUP
	  case "1010":
		  return "manageContentForwardSpace('SOCIALM', '"+ contentId+"', '"+ contentType+"', '"+ $.trim(replaceAll(a1,",",""))+"')";
		  break;

	  // SOCIAL_AREA_NORMAL
	  case "1005":
		  return "manageContentForwardSpace('SOCIALM', '"+ contentId+"', '"+ contentType+"', '"+ $.trim(replaceAll(a1,",",""))+"')";
		  break;
	  
	  // SOCIAL_AREA_INITIATIVE
	  case "1011": 
		  return "manageContentForwardSpace('SOCIALM', '"+ contentId+"', '"+ contentType+"', '"+ $.trim(replaceAll(a1,",",""))+"')";
		  break;
		
      // MODEL
	  case "50": 
		  if (scope == 'REPORTS'){
			  //csMLO = owner;
			  return "changeModelReportSection('"+owner+"', '"+a1+"',event);return false;";
		  }
		  
		  if (scope == 'MASHBOARDS'){
			  //csMLO = owner;
			  return "changeModelDashboardSection('"+owner+"', '"+a1+"');return false;";
		  }
		  
		  break;
		  
	  default:
		 // alert('CassiopeaType non contemplato:' + contentType);
	  }
  }
  
  function getLinkForContent(owner, dshID, contentId, contentType, versionNumber, initialStatus, noRenderIds, autoFocus, targetUid){
	  var contentIdParam = "";
	  var versionNumParam = "";
	  var initialStatusParam = "";
	  var contentTypeParam = "";
	  var noRenderListParam = "";
	  var autoFocusParam = "";
	  var targetUidParam = "";
	  if (contentType != null && contentType != "") {
		  contentTypeParam = "&FORCED_CONTENT_TYPE="+contentType;
	  }
	  if (contentId != null && contentId != "") {
		  contentIdParam = "&FORCED_CONTENT_ID="+contentId;
	  }
	  if (versionNumber != null) {
		  versionNumParam = "&VERSION_NUMBER="+versionNumber;
	  }
	  if (initialStatus != null && initialStatus != "") {
		  initialStatusParam = "&INITIAL_STATUS="+initialStatus;
	  }
	  if (noRenderIds != null) {
		  if (Array.isArray(noRenderIds)) {
			  for (var idx in noRenderIds) {
				  noRenderListParam += "&NO_RENDER_ELEM_" + noRenderIds[idx] + "=true";
			  }
		  }
		  else {
			  noRenderListParam = "&NO_RENDER_ELEM_" + noRenderIds + "=true";
		  }
		  
	  }
	  if (autoFocus != null && autoFocus != '') {
		  autoFocusParam = '&DCY~focusCtx='+autoFocus;
	  }
	  if (targetUid != null && targetUid != '') {
		  targetUidParam = '&DCY~targetUid='+targetUid;
	  }
	 
	  return '../dsh/dshPage.jsp.cas?revent=LW_DSH_LINK&dshID='+dshID+'&csMLO='+owner+contentIdParam+versionNumParam+initialStatusParam+contentTypeParam+noRenderListParam+autoFocusParam+targetUidParam;
  }
  
  function manageContentForward(owner, dshID, contentId, contentType, versionNumber, initialStatus, noRenderIds){
	  return rootPage.navigateToContent(getLinkForContent(owner, dshID, contentId, contentType, versionNumber, initialStatus, noRenderIds));
  }
  
  function manageContentForwardFromSpace(owner, dshID, contentId, contentType, versionNumber, initialStatus, noRenderIds, spaceId){
	  return rootPage.navigateToContent(getLinkForContentFromSpace(owner, dshID, contentId, contentType, versionNumber, initialStatus, noRenderIds, spaceId));
  }
  
  function getLinkForContentFromSpace(owner, dshID, contentId, contentType, versionNumber, initialStatus, noRenderIds, spaceId){
	  var link = getLinkForContent(owner, dshID, contentId, contentType, versionNumber, initialStatus, noRenderIds);
	  link += "&associatedAreas=" + spaceId;
	  
	  return link;
  }
  
  /**
   * Funzione sostituita con quella sotto per risoluzione ticket n.#108056 
   * TODO : PF/FF : Da rivedere gestione nei documenti
   */
  /*function manageContentForwardWidget(scope, owner, dshID, contentId, contentType, versionNumber, initialStatus, noRenderIds) {
	  var randomIdOperation = getRandomString();
	  var url = getLinkForContent(owner, dshID, contentId, contentType, versionNumber, initialStatus, noRenderIds);
	  var dialog = jQDialogIframe("jqdScWidget_"+randomIdOperation, "", url, "900", "700", "refreshMashbordPostModify('jqdScWidget_"+randomIdOperation+"','"+scope+"')", true, true, true, false);
	  setJQDialogDataValue("jqdScWidget_"+randomIdOperation, "contentUID", owner + contentId);
  }*/
  
  function manageContentForwardWidget(scope, owner, dshID, contentId, contentType, versionNumber, initialStatus, noRenderIds, autoFocus, targetUid) {	  
	  var url = getLinkForContent(owner, dshID, contentId, contentType, versionNumber, initialStatus, noRenderIds, autoFocus, targetUid);
	  
	  var $jqDialog = rootPage.$('#jqdScWidget');
	  
	  if (dialogExists($jqDialog)){
		  var iframe = "<iframe id='jqDialogIframe_jqdScWidget' name='jqDialogIframe' class='jqDialogIframe' src='"+url+"' frameborder='0' style='width:100%;height:99%;scrolling='no'></iframe>";
		  $jqDialog.html(iframe).dialog("open");
		  $jqDialog.removeData("contentUID").data("contentUID", owner + contentId);
	  }
	  else{
		  jQDialogIframe("jqdScWidget", "", url, "900", "700", "refreshMashbordPostModify('jqdScWidget','"+scope+"')", true, true, true, false);	
		  $jqDialog.removeData("contentUID").data("contentUID", owner + contentId);
	  }
	  
  }
  
  function manageContentForwardWidgetReport(owner, reportID, autoFocus, targetUid) {
	  var url = '../report/reportLauncher.jsp?idReportRequest=' + reportID + '&csMLO=' + owner;
	  if (autoFocus != null && autoFocus != '') {
		  url += '&DCY~focusCtx='+autoFocus;
	  }
	  if (targetUid != null && targetUid != '') {
		  url += '&DCY~targetUid='+targetUid;
	  }
	  jQDialogIframe("jqdScWidget", "", url, "900", "700", "", true, true, true, false);
	  setJQDialogDataValue("jqdScWidget", "contentUID", owner + reportID);
  }
  
  function reloadWinOpener(jqDialogId) {
	  var winOpener = rootPage.getWindowReferenceFromRootPage(jqDialogId);
	  var $jsDialog = rootPage.$('body').find("#"+jqDialogId);
	  
	  if(!$jsDialog.length){
		  return;
	  }
	  
	  var taskChangedStatus = $jsDialog.data("taskChangedStatus");
	  
	  if(taskChangedStatus){
		 setTimeout(function(){ winOpener.location.reload(true);},500);
	  }
  }
  
  function refreshMashbordPostModify(dlgId, scope) {
	  if (scope == 'WIDGET' || scope == 'CLIST' || scope == 'TLIST' || scope == 'ANNOTATION' || scope == 'WELCOME' || scope == 'REPORT') {
		  try {
			  var contentDialog = getWindowReferenceFromRootPage(dlgId);
			  if (contentDialog) {
				  if (scope == 'WELCOME') {
					  // refresh welcome
					  if (contentDialog.isMashbordModified == true) {
						  rootPage.showModelSection();
					  }
				  }
				  else {
					  contentDialog.reloadMashboard();
				  }
			  }
		  }catch(e) {}
	  } 
  }
  
  function manageContentForwardReport(owner, reportID) {
	  return rootPage.navigateToContent('../report/reportLauncher.jsp?idReportRequest=' + reportID + '&csMLO=' + owner);
  }
  
  function manageContentForwardTask(owner, taskID, scope, reportViewId) {
	  var jqDialogTaskId = 'jqDialogTask';
	  var controllerUid = "task";
	  
	  var contextData = new ctxManager()
	  		.set("owner", owner)
	  		.set("taskId", taskID)
	  		.set("taskAction", 1);
	  
	  if (isDefined(reportViewId)) {
		  contextData.set("idReportRequest", reportViewId);
	  }
	  
	  var callBackBeforeClose = (isDefined(scope) && scope == 'WELCOME') ? "reloadWinOpener('"+jqDialogTaskId+"')" : "refreshMashbordPostModify('"+jqDialogTaskId+"', '"+scope+"')";
	  
	  createJQDialogByDPrWithCallbackClose(jqDialogTaskId, controllerUid, contextData.toString(), getLabel('taskDlgTitle'), "670", "520", callBackBeforeClose);
  }
  
  function manageContentForwardAnnotation(owner, annotationID, scope, reportViewId, refObjKeyDesc) {
	  var jqDialogDiscussionId = 'jqDialogDiscussion';
	  var controllerUid = "annotationDiscussions";
	  
	  var contextData = new ctxManager()
	  		.set("owner", owner)
	  		.set("discRefObjId", annotationID)
	  		.set("discRefObjType", "371")
	  		.set("discussionType", "3")
	  		.set("discContextRootId", annotationID)
	  		.set("createDiscussion", true, true)
	  		.set("createMessage", true);
	  
	  if (isDefined(reportViewId) && reportViewId!='') {
		  contextData.set("idReportRequest", reportViewId);
	  }
	  
	  var dialogTitle = getLabel('discussion');
	  if (isDefined(refObjKeyDesc) && refObjKeyDesc!='') {
		  var refObjContextDs = JSON.parse(refObjKeyDesc);
		  var title = refObjContextDs.title;
		  dialogTitle = dialogTitle + ": Report '" + title + "'" ;
		  
		  var data = refObjContextDs.data;
		  if (data != null) {
			  var desc = data[0];
			  for (var i=1;i<data.length;i++)
				  desc += " - " + data[i];
			  desc = " > " + desc;
			  dialogTitle = dialogTitle + desc;
		  }
	  }
	

	  createJQDialogByDPrWithCallbackClose(jqDialogDiscussionId, controllerUid, contextData.toString(), dialogTitle, "670", "520", "refreshWindowOpener('"+jqDialogDiscussionId+"')");
  }
  
  function manageContentForwardSignal(owner, signalID, scope) {
	  var jqDialogSignalId = "jqDialogSignal";
	  var controllerUid    = "signalDiscussion";
	  
	  var contextData = new ctxManager()
	  		.set("owner", owner)
	  		.set("createSignal", false)
	  		.set("objectID", signalID)
	  		.toString();
	  
	  createJQDialogByDPrWithCallbackClose(jqDialogSignalId, controllerUid, contextData, "", "800", "600", "refreshMashbordPostModify('"+jqDialogSignalId+"', '"+scope+"')");
  }
  
  function autoOpenObject(UID, reportViewId, refObjKeyDesc) {
	  var list = UID.split("_");
	  if (list.length == 3) {
		  var owner = list[0];
		  var type  = list[1];
		  var id    = list[2];		  
	  }
	  else {
		  // Impossibile aprire l'oggetto, dati insufficenti
		  return;
	  }

	  try {
		  switch (type) {
		  case "371":
			   manageContentForwardAnnotation(owner, id, 'REPORT', reportViewId, refObjKeyDesc);
			  break;
		  case "372":
			  manageContentForwardTask(owner, id, 'REPORT', reportViewId);
			  break;
		  case "1007":
			  manageContentForwardSignal(owner, id, 'REPORT');
			  break;
		  }
	  } 
	  catch (e) {}
  }
  
  var qsParams = {
			qsObject : new Array(),
			init: function() {
				return this;
			},
			set: function(id, value) {
				for(var i in this.qsObject) {
					if(this.qsObject[i].id == id) {
						this.qsObject[i].value = value;
				  			return this;
				  		}
				  	}
				var qsParam = {};
				qsParam.id = id;
				qsParam.value = value;
				this.qsObject.push(qsParam);
				return this;
			},
			toString: function() {
				var str = "";
				for(var i in this.qsObject)
					str += "&" + this.qsObject[i].id + "=" + this.qsObject[i].value;
				return str;
			}
  };

  function getJqDialogJSCode(dataProviderId, ctx, title, width, height) {
	  var fun = "createJQDialogByDPrWithOptions(";
	  fun += "$(this).attr('id'), ";
	  fun += getParamFunctionByType(dataProviderId) + ", ";
	  fun += "'" + ctx.replace(/\"/g,"\\'") + "', ";
	  fun += getParamFunctionByType(title) + ", ";
	  fun += getParamFunctionByType(width) + ", ";
	  fun += getParamFunctionByType(height);
	  fun += ");";
	  return fun;
  }

  function renderizerTemplate(json, parentNode, localVars){
	  var result = rootPage.templateRenderizer(json, localVars);
	 
	  decorateElementContent(parentNode, result, json);
	 
	  evaluateJavascript(parentNode);
  };
  
  function getColHeaderIndex(table) {
	  var rowHeader = null;
	  var map = new Array();
	  for (var i=0; i<table.length; i++) {
		  var row = table[i];
		  var find = false;
		  for (var j=0; j<row.cells.length; j++) {
			  var cell = row.cells[j];
			  if (cell.type && cell.type == "C") {
				  find = true;
				  break;
			  }
		  }
		  		  
		  if (find) {
			  rowHeader = row.cells;
			  break;
		  }
	  }
	  
	  var aux = "";
	  
	  if (rowHeader != null) {
		  for (var i=0; i<rowHeader.length; i++) {
			  var cell = rowHeader[i];
			  if (cell.type && (cell.type == "H" || cell.type == "C")) {
				  map[cell.viewVal] = i;
			  }			  
		  }		  
	  }	  
	  
	  return map;
  }
    
  /**
   * Effettua la chiamata al dataprovider specificato richiamando la funzione di callback richiesta,
   * consentendo di specificare un contesto per la stessa
   * 
   * @author Costantini Simone
   * @param uid
   * @param context
   * @param callback
   * @param contentUID
   */
  function requestProviderDataForContentUID(context, selector, controllerUid, callback, contentUID){
  	$.ajax({
  		data: {revent:'GET_DATA',a0:controllerUid,a1:context},
  		success: function(response, textStatus, xhr) {
  			callback(response, selector, controllerUid, context, contentUID);
  		},
  		error: function(response, textStatus, xhr) {
  			logToConsole('requestProviderDataForContentUID AJAX Error:' + response.responseText);
  		}
  	});
  }
  
/**
 * Chiamata helper per azioni asincrone all'interno del template
 * @param revent --> handler da chiamare
 * @param dataProviderDelegatedFunction --> funzione per l'aggiornameto del #data
 * @param refresh --> se true viene ricaricato il template
 * @param viewToSwitch --> nome della view di uscita
 * @param currentNode --> aggiornamento della view su nodo jquery diverso da this
 * @returns {String} --> Renderizzazione della funzione
 */
function getAjaxCall(revent, dataProviderDelegatedFunction, refresh, viewToSwitch, currentNode, actionOnSuccess) {
	if (!currentNode)
		currentNode = 'this';
	var fun = "handleAjaxCall(" + currentNode + ",";
	fun += "'" + revent + "'";
	if (dataProviderDelegatedFunction != null)
		fun += ", '" + replaceApexWithDouble(dataProviderDelegatedFunction) + "'";
	else
		fun += ", null";
	fun += ", " + refresh;
	if (viewToSwitch != null)
		fun += ", '" + viewToSwitch + "'";
	else
		fun += ", null";
	if (actionOnSuccess != null)
		fun += ", '" + actionOnSuccess + "'";
	else
		fun += ", null";
	fun += ");";
	return fun;
}
  
/**
 * Ricostruisce il #data in base alle regole della dinzione dataProviderDelegatedFunction e chiama un handler java
 * Se è impostato il refresh viene ricaricato il template.
 * @param currentNode
 * @param reventAction
 * @param dataProviderDelegatedFunction
 * @param refresh
 */
function handleAjaxCall(currentNode, reventAction, ctxDelegatedFunction, refresh, viewToSwitch, actionOnSuccess, actionOnError, beforeCallBack) {
	  var dataNode = getCtxNode(currentNode);
	  var selector = "#"+dataNode.parent().attr('id');
	  var ctx = getCtx(currentNode);
	  
	  var ctxProvidedByDelegateFun = cloneObject(ctx);
	  var successCtx;
	  if (ctxDelegatedFunction != null) {
		  successCtx = cloneObject(ctx);
		  successCtx = eval(ctxDelegatedFunction + '(ctxProvidedByDelegateFun,successCtx)');
	  } else {
		  successCtx = ctxProvidedByDelegateFun;
	  }
	  var rasterCtxProvidedByDelegateFun = getRasterCtxContainer(ctxProvidedByDelegateFun);
	  $.ajax({
		  data: {revent:reventAction,a0:rasterCtxProvidedByDelegateFun},
		  beforeSend: function( jqXHR, settings ) {
				if(isDefined(beforeCallBack)){
					beforeCallBack();
				}
			},
		  success: function(response, textStatus, xhr) {
			  if (refresh == true) {
				  var contrUID = getCtxValue("contrUID", ctx);
				  if (contrUID=="")
					  contrUID = getCtxValue("contrUID", successCtx);
				  renderProviderDataForCtxByView(successCtx, selector, contrUID, viewToSwitch);
			  } 
			  if (actionOnSuccess != null) {
				  if (typeof actionOnSuccess == "function")
					  actionOnSuccess();
				  else
					  eval(actionOnSuccess);
			  }
		  },
		  error: function(jqXHR, textStatus, errorThrown){
			  $.ajaxSetup()['error'](jqXHR, textStatus, errorThrown);
			  if (actionOnError)
				  actionOnError();
		  }
	  });
}

/**
 * Chiamata helper che stampa il codice per lo svitch a un altro template
 * @param viewToSwitch
 * @param ctxKeyToModify --> delta di modifica del ctx es. "[{'id': 'status','value': 'edit'}]"
 * @returns {String}
 */
function getJScodeToSwitchView(viewToSwitch, ctxKeyToModify, actionOnSwitch) {
	  var fun = "localSwitchToView(this,";
	  fun += "'" + viewToSwitch + "',";
	  fun += ctxKeyToModify;
	  if (actionOnSwitch != null) {
		  fun += ",'" + actionOnSwitch + "'";
	  }
	  fun += ");";
	  return fun;
}

/**
 * Funzione che esegue lo switch ad un altro template, usa il contesto di uscita
 * @param currentNode --> nodo corrente
 * @param viewToSwitch --> view in cui fare lo swith
 * @param ctxKeyToModify --> delta di modifica del ctx es. "[{'id': 'status','value': 'edit'}]"
 */
function localSwitchToView(currentNode, viewToSwitch, ctxKeyToModify, actionOnSwitch) {
	
	if (actionOnSwitch != null) {
		  eval(actionOnSwitch);
	}
	
	var currentCtx = getCtx(currentNode);
	
	if (ctxKeyToModify != null && ctxKeyToModify.constructor == Array) {
		var ctxMng = new ctxManager(currentCtx);
		for(var i in ctxKeyToModify) {
			ctxMng.set(ctxKeyToModify[i].id, ctxKeyToModify[i].value);
		}
		currentCtx = ctxMng.toObject();
	}
	
	var dataNode = getCtxNode(currentNode);
	var id = dataNode.parent().attr('id');
	var contrUID = getCtxValue("contrUID", currentCtx);
	
	renderProviderDataForCtxByView(currentCtx, "#"+id, contrUID, viewToSwitch);
}

/**
 * Effettua la chiamata al dataprovider riaggiornando il template,
 * usando il solo contesto di ingresso iniziale, per il primo nodo trovato a partire da quello indicato.
 * 
 * @author Costantini Simone
 */
function reloadTemplate(selector, specificView) {
	var obj = $("dcyContent:first", $(selector));
	var nodeInfo = getNodeInfoFromNode(obj);
	var ctxIn = nodeInfo.ctxIn;
	var controllerUid = nodeInfo.controllerUid;
	if (ctxIn == null)
		ctxIn = [];
	
	renderProviderDataForCtxByView(ctxIn, selector, controllerUid,specificView);
}


/**
 * Ritorna una stringa tradotta a partire dall'id della label
 * @author Costantini Simone
 * @param labelID
 * @returns
 */
function getEncodedLanguageString(labelID){
	return LANGUAGES.getLabelHTMLEncode(labelID);
}

function escapeString(str){
	var toReturn = encodeApex(str);
	return toReturn.replace(/\n/g, '<br>');
}

/**
 * Ritorna il context container per la gestione di content based template
 * @param rasterCtxData
 * @returns
 */
function contentBasedTemplateCtx(rasterCtxData){
	try {
		var previewRasterContext = JSON.parse(rasterCtxData);
		return JSON.parse(getCtxValue("contentPreview", previewRasterContext.ctx));
	} catch(e) {
		return {"content": rasterCtxData};
	}
}

function toStringone(str) {
	return str.toString();
}

/**
 * Ritorna la view per la gestione di content based template
 * @param rasterCtxData
 * @param prefix
 * @param postfix
 * @returns {String}
 */
function contentBasedTemplateNode(rasterCtxData, prefix, postfix){
	try {
		var previewRasterContext = JSON.parse(rasterCtxData);
		return "" + prefix + getCtxValue("dataType", previewRasterContext.ctx) + postfix;
	} catch(e) {
		return prefix + "DEFAULT" + postfix;
	}
}

/**
 * Ritorna la view per la gestione di content based template con specifica del campo
 * all'interno del ctx
 * @param rasterCtxData
 * @param descField
 * @param prefix
 * @param postfix
 * @returns {String}
 */
function contentBasedTemplateNodeSpec(rasterCtxData, descField, prefix, postfix){
	try {
		var previewRasterContext = JSON.parse(rasterCtxData);
		return "" + prefix + getCtxValue(descField, previewRasterContext.ctx) + postfix;
	} catch(e) {
		return prefix + "DEFAULT" + postfix;
	}
}

/**
 * Stampa il codice per la creazione di una jqDialog a cui sia passato il ctx corrente
 * @param dataProviderId
 * @param title
 * @param width
 * @param height
 * @returns {String}
 */
function getJqdialogCurrentCtxCode(dataProviderId, title, width, height) {
	var fun = "getJqdialogCurrentCtx(this,";
	fun += getParamFunctionByType(dataProviderId) + ", ";
	fun += getParamFunctionByType(title) + ", ";
	fun += getParamFunctionByType(width) + ", ";
	fun += getParamFunctionByType(height);
	fun += ");";
	return fun;
}

/**
 * Apre una jqDialog generata con il ctx corrente
 * @param currentNode
 * @param dataProviderId
 * @param title
 * @param width
 * @param height
 */
function getJqdialogCurrentCtx(currentNode, dataProviderId, title, width, height) {
	var currentCtx = getCtx(currentNode);
	var rasterCtx = getRasterCtxContainer(currentCtx);
	var idDialog = $(this).attr('id');
	if (!idDialog)
		idDialog = dataProviderId;
	rootPage.createJQDialogByDPrWithOptions(idDialog, dataProviderId, rasterCtx, title, width, height);
}

//get a localized label by the key.
function getLabel(labelName, postfix) {
	if (isDefined(postfix)) {
		return LANGUAGES.getOriginalLabel(labelName+"_"+postfix);		
	}
	else {
		return LANGUAGES.getOriginalLabel(labelName);
	}
}

//get a localized label by the key encoding html characters.
function getLabelHTMLEncode(labelName) {
	return LANGUAGES.getLabelHTMLEncode(labelName);
}

//get a localized label by the key replacing double quote.
function getLabelWithoutDoubleQuote(labelName) {
	return LANGUAGES.getLabelWithoutDoubleQuote(labelName);
}

// get a localized label by the key replacing single quote.
function getLabelWithoutSingleQuote(labelName) {
	return LANGUAGE.getLabelWithoutSingleQuote(labelName);
}

function execQButton(objectJsID, executionParams) {
	var fun = "execQueryButton(";
	fun += "'" + objectJsID + "', ";
	fun += "'" + executionParams + "');";
	return fun;
}

function removeCssClass(cssClass) {
	return "$(this).removeClass('" + cssClass + "');";
}

/**
 * Da richiamare esclusivamente come helper
 */
function sendParCh(pValue) {
	var dId = getCtxValue('mshPageId', this.ctx.ctx);
	var pId = getCtxValue('refObjId', this.ctx.ctx);
	var pName = getCtxValue('formName', this.ctx.ctx);
	
	return "sendParamChanged('"+dId+"','"+pId+"','"+pName+"','"+pValue+"');";
}


/**
 * Da richiamare esclusivamente come helper
 */
function sendMultiParCh(selectorId) {
	var mshPageId = getCtxValue('mshPageId', this.ctx.ctx);
	var refObjId = getCtxValue('refObjId', this.ctx.ctx);
	var exportType = getCtxValue('exportType', this.ctx.ctx);
	var formName = getCtxValue('formName', this.ctx.ctx);
	
	return "sendMultipleParamChangedForStyled('"+selectorId+"','"+refObjId+"','"+mshPageId+"','"+exportType+"','"+formName+"');";
}

function getNotificationSelectCode(dId, pId, notId, selValue) {
	if (selValue) {
		selValue = getDiscussionCtxForNotification(selValue, notId);
		selValue = selValue.replace(/\\\"/g,"\\\\\\'").replace(/\"/g,"\\'").replace(/\n/g, "");
	}
	return "notificationSelect('" + dId + "', '" + pId + "', this, '" + notId + "', '" + selValue + "')";
}

function toFunction() {
	if (typeof this.data.funct != 'undefined')
		return this.data.funct + (this.data.funct.indexOf("(")<0?"(this, event)":"");
	
	return "";
}

function getDiscussionCtxForNotification(rasterEventDataCtx, notId){
	// recupera i valori di contesto necessari dall'eventData.
	var eventDataCtxCont = getValidJsonContent(rasterEventDataCtx);
	if (eventDataCtxCont) {
		// crea il contesto da utilizzare come custom context di una discussion.
		var discCtxObj = buildNewCtxObject();
		// copia i dati di contesto utili
		copyContextData("owner", eventDataCtxCont.ctx, discCtxObj);
		copyContextData("discussionId", eventDataCtxCont.ctx, discCtxObj);
		copyContextData("discussionType", eventDataCtxCont.ctx, discCtxObj);
		copyContextData("secToken", eventDataCtxCont.ctx, discCtxObj);
		addDataToCtx(discCtxObj, "notId", notId);  
		// restituisce il raster context
		var rasterDiscCtxCont = getRasterCtxContainer(discCtxObj);
		return rasterDiscCtxCont;
	}
	return "";
}

function fix_ie_css_limit(){	
	logToConsole('fix css limit '+document.styleSheets.length);    
    if (document.styleSheets.length < 31) return;
    var tmpA = document.createElement("a"),
        links = $("link[rel='stylesheet']"),
        paths = links.map(function () {
            var mediaAttr = this.getAttribute("media");
            if (!mediaAttr || (mediaAttr.indexOf("print") == -1)) {
                tmpA.href = this.href; // workaround for obtaining an absolute URL (independent of where the @import directive will be placed)
                return tmpA.href;
            }
        }).get(),
        len = paths.length,
        importsPerStyle = 15; // a number of @import directives per one CSS file
    links.slice(Math.ceil(len / importsPerStyle)).remove();
    links = $("link[rel='stylesheet']");
    links.each(function () {
        if (this.styleSheet && this.styleSheet.cssText != "") {
            this.styleSheet.cssText = "";
        }
    });
    for (var i = 0; i < len; i++) {
        try{ // try/catch necessario per ie8 altrimenti blocca l'esecuzione degli script successivi
        	links[Math.floor(i / importsPerStyle)].styleSheet.addImport(paths[i]);
        }catch(e){logToConsole('Errore JS '+ e.name + ' - ' + e.message);}
    }
    
};

function getFileTypeIcon(fileName) {
	var ext = fileName.substr(fileName.lastIndexOf(".")+1).toLowerCase();
	
	var DEFAULT = "fileSconosciuto.gif";
	


	switch (ext){
		case "7z":
			return "7z.gif";
			
		case "avi":
			return "avi.png";
			
		case "access":
			return "access.gif";
						
		case "bmp":
			return "bmp.gif";
			
		case "dmg":
			return "dmg.gif";
			
		case "doc":
		case "dot":
		case "docx":
		case "dotx":
			return "doc.gif";
			
		case "exe":
		case "bat":
		case "com":
			return "exe.gif";
			
		case "fla":
		case "swd":
			return "fla.gif";
			
		case "gif":
			return "gif.gif";
			
		case "htm":
		case "html":
		case "shtml":
			return "html.gif";
			
		case "jpg":
		case "jpeg":
		case "jpe":
			return "jpeg.gif";
			
		case "js":
			return "js.gif";
			
		case "mail":
			return "mail.gif";
			
		case "mid":
		case "midi":
			return "midi.gif";
			
		case "mov":
			return "mov.gif";
			
		case "mp3":
			return "mp3.gif";
			
		case "mpg":
		case "mpeg":
		case "mpe":
			return "mpg.gif";

		case "mpp":
			return "mpp.gif";
			
		case "newwin":
			return "newWin.gif";
			
		case "pdf":
			return "pdf.gif";
			
		case "png":
			return "png.gif";

		case "ppt":
		case "pptx":
		case "pps":
		case "pot":
			return "ppt.gif";
			
		case "properties":
			return "properties.gif";
			
		case "pub":
			return "pub.gif";
			
		case "qt":
			return "qt.gif";
			
		case "rar":
			return "rar.gif";
			
		case "rss":
			return "rss.gif";
			
		case "rtf":
			return "rtf.gif";
			
		case "sit":
			return "sit.gif";
			
		case "swf":
			return "swf.gif";
			
		case "txt":
			return "txt.gif";
			
		case "vsd":
			return "vsd.gif";
			
		case "xl":
		case "xls":
		case "xlsx":
		case "xlv":
		case "xla":
		case "xlb":
		case "xlt":
		case "xlm":
		case "xlk":
		case "xlw":
		case "csv":
			return "xls.gif";
			
		case "xml":
			return "xml.gif";
			
		case "zip":
		case "gzip":
			return "zip.gif";
			
		default : 
			return DEFAULT;
	}
}

//Taggable menu functions
function toSingleMenu(obj) {

	var ctx = getCtx(obj);
	
	var targetSelector = getCtxValue("targetSelector", ctx);
	var preferredDP = $(obj).attr("provider");
		
	var ctxToSend = [];
	addDataToCtx(ctxToSend, 'isForTabbedMenu', getCtxValue("isForTabbedMenu", ctx));
	addDataToCtx(ctxToSend, 'textFilter', getCtxValue("textFilter", ctx));
	addDataToCtx(ctxToSend, 'textFilterOrig', getCtxValue("textFilterOrig", ctx));
	addDataToCtx(ctxToSend, 'targetSelector', targetSelector);
	addDataToCtx(ctxToSend, 'decoratorPrefix', getCtxValue("decoratorPrefix", ctx));
	addDataToCtx(ctxToSend, 'decoratorAttributes', getCtxValue("decoratorAttributes", ctx));
	addDataToCtx(ctxToSend, 'associatedDP', getCtxValue("associatedDP", ctx));
	addDataToCtx(ctxToSend, 'preferredDP', preferredDP);
	
	var ctxObject = {'ctx' : ctxToSend };
	
	var $rootNode = $(targetSelector);
	$rootNode.tagCollector('setOption', 'preferredDP', preferredDP);
	
	createJQTooltipByDataProviderWithHeight($rootNode, "taggableItems" , JSON.stringify(ctxObject), 300);
}

function toMultipleMenu(obj) {
	var ctx = getCtx(obj);
	
	var targetSelector = getCtxValue("targetSelector", ctx);
		
	var ctxToSend = [];
	addDataToCtx(ctxToSend, 'isForTabbedMenu', getCtxValue("isForTabbedMenu", ctx));
	addDataToCtx(ctxToSend, 'textFilter', getCtxValue("textFilter", ctx));
	addDataToCtx(ctxToSend, 'textFilterOrig', getCtxValue("textFilterOrig", ctx));
	addDataToCtx(ctxToSend, 'targetSelector', targetSelector);
	addDataToCtx(ctxToSend, 'decoratorPrefix', getCtxValue("decoratorPrefix", ctx));
	addDataToCtx(ctxToSend, 'decoratorAttributes', getCtxValue("decoratorAttributes", ctx));
	addDataToCtx(ctxToSend, 'associatedDP', getCtxValue("associatedDP", ctx));
	addDataToCtx(ctxToSend, 'preferredDP', null);
	
	var ctxObject = {'ctx' : ctxToSend };
	
	var $rootNode = $(targetSelector);
	$rootNode.tagCollector('setOption', 'preferredDP', null);
	
	createJQTooltipByDataProviderWithHeight($rootNode, "taggableItems" , JSON.stringify(ctxObject), 300);

}
	
/**
 * Renderizza un contenitore con lo script necessario per renderizzare informazioni pr
 * i dati provenienti da un data provider
 * 
 * @param ctx contesto in formato oggetto nodo
 * @param contrId identificatore del controller da invocare
 * @param nodeId identificatore da applicare al contenitore che ospiterà il contenuto renderizzato.
 * @returns
 */
function renderNodeToRenderProviderData(ctx, contrId, nodeId, cssClass) {
	var rasterCtx = getRasterCtxContainer(ctx);
	var cssClassAttr = "";
	if (cssClass!=null && cssClass!="")
		cssClassAttr = "class='" + cssClass + "'";
	
	var out = "<div id='"+ nodeId + "' " + cssClassAttr + " >";
	out += "<div style='display:none;' id='"+ nodeId + "_TMP'>" +  getHTMLEncode(rasterCtx)  + "</div>";
	out += "<script>renderProviderDataForContextTo($('#"+nodeId+"_TMP').text(), '#"+nodeId+"','"+contrId+"');</script></div>";
	
	return out;
	
}

/**
 * Clona il contesto e ne restiruisce un altro per le discussion sotto a un social content o un task.
 * 
 * @param ctx
 * @returns
 */
function manageCtxForDiscussion(ctx) {
	var ctxObject = cloneCtxObject(ctx);
	//mainObjId
	var refObjId = getCtxValue('refObjId', ctxObject);
	removeDataFromCtx('refObjId', ctxObject);
	addDataToCtx(ctxObject, 'mainObjId', refObjId);
	// view
	var discView = getCtxValue('discViewTMPL', ctxObject);
	removeDataFromCtx('discViewTMPL', ctxObject);
	addDataToCtx(ctxObject, 'viewTMPL', discView);
	
	return ctxObject;
}

function manageCtxForSocialBar(ctx) {
	
	var owner = getCtxValue('owner', ctx);
	var containerId = getCtxValue('forcedObjectId', ctx);
	if (!containerId || containerId == "")
		containerId = getCtxValue('objectID', ctx);
	if (!containerId || containerId == "")
		containerId = getCtxValue('containerId', ctx);
	var forcedContentType = getCtxValue('forcedContentType', ctx);
	
	var ctx = buildNewCtxObject();
	addDataToCtx(ctx, 'owner', owner);
	addDataToCtx(ctx, 'refObjId', containerId);
	addDataToCtx(ctx, 'refObjectType', forcedContentType);
	return ctx;
}

function manageCtxForTaskSocialBar(ctx) {
	var owner = getCtxValue('owner', ctx);
	var taskId = getCtxValue('taskId', ctx);
	var taskType = getCtxValue('objectType', ctx);
	
	var ctx = buildNewCtxObject();
	addDataToCtx(ctx, 'owner', owner);
	addDataToCtx(ctx, 'refObjId', taskId);
	addDataToCtx(ctx, 'refObjectType', taskType);
	return ctx;
}

function manageCtxForSignalSocialBar(ctx) {
	var owner = getCtxValue('owner', ctx);
	var taskId = getCtxValue('discRefObjId', ctx);
	var taskType = getCtxValue('discRefObjType', ctx);
	
	var ctx = buildNewCtxObject();
	addDataToCtx(ctx, 'owner', owner);
	addDataToCtx(ctx, 'refObjId', taskId);
	addDataToCtx(ctx, 'refObjectType', taskType);
	return ctx;
}

/**
 * helper di default per eseguire qualunque funzione.
 * 
 * @param funct
 * @returns
 */
function execute(funct, par1, par2, par3, par4, par5, par6, par7, par8, par9) {
	return eval(funct+'(par1, par2, par3, par4, par5, par6, par7, par8, par9)');
}

/**
 * Renderizza un nodo pretty abbr per visualizzare una pretty date a a partire da un bean sia in formato raster che in formato nodo. 
 * 
 * @param prettyDateBean bean in formato raster o nodo contentente le informazioni per mostrare la data in pretty date.
 * @returns
 */
function renderPrettyDate(prettyDateBean) {
	if (isDefined(prettyDateBean)) {
		var pdb = getValidJsonContent(prettyDateBean);
		return renderPrettyAbbrNode(pdb.title, pdb.date);
	}
	return "";
}

/**
 * Renderizza un nodo pretty abbr per visualizzare una pretty date
 * 
 * @param title etichetta da visualizzare sull'over. Es. 'Tuesday 5 February 2013 13:20:18'
 * @param data data effettiva che il motore del pretty date considera per fare i calcoli. Es.'Tue Feb 5 2013 13:20:18 +0100'
 * @returns
 */
function renderPrettyAbbrNode(title, data) {
	var abbrNode = document.createElement('abbr');
	abbrNode.setAttribute('class', 'pydata de_cursorDefault');
	abbrNode.setAttribute("onmouseover", "setJQtipOnElement(this,'"+title+"','dates');stopPropagation(event);return false;");
	abbrNode.setAttribute('data', data);
	return getOuterHTML(abbrNode);
}

/**
 *  Get the element into which the custom tag is inserted.
 *  If the element is a dashboard element, the node, into which the custom tag is insered, is the div with id = "#OBJ"+<idElement>+"_BODY".
 *  This is necessary to not overwrite the element title node.
 * @param element
 */ 
function getElementIntoWhichTheCustomTagIsInsered(element){	
	var dshBodyElement = $('#'+element.attr('id')+'_BODY', $(element));
	if (dshBodyElement.size()>0 && dshBodyElement.hasClass("dshObjBody"))
		return dshBodyElement;
	
	return element;
};
/**
 * Return the page param value reading from the context attribute called "pageParams"
 * @param paramName
 * @param pageParams
 * @returns
 */
function getPageParamValue(paramName, pageParams){
	var parsePageParams = JSON.parse(pageParams);
	for(var x in parsePageParams){
		if (x == paramName){
			return parsePageParams[x][0];
		}
	}
	return "";
};

/**
 *  Return the switch view action 
 * @param mshPageId
 * @param pageParams: the context attribute called "pageParams"
 * @param paramName
 * @returns {String}
 */
function getSwitchViewAction(mshPageId, pageParams, paramName){		
	var viewToSwitch = getPageParamValue(paramName, pageParams);
	var crosstabId = getPageParamValue('CROSSTAB_ID', pageParams);
	return "switchViewCrosstabAndSendParamChanged('"+mshPageId+"','"+crosstabId+"','"+viewToSwitch+"');";
};

/**
 * Return the context attribute value reading from the specificInfo attribute value
 * @param ctxString
 * @param childAttrName
 * @returns {String}
 */
function getCtxAttrValueFromSpecificInfo(ctxString, childAttrName, noEncode){
	try {
		var parentCtxAttrValue = getCtxAttrValueFromCtxString(ctxString, 'specificInfo');

		if (isDefined(noEncode) && noEncode) { 
			return (parentCtxAttrValue!= '' && parentCtxAttrValue != null) ? eval("JSON.parse(parentCtxAttrValue)."+childAttrName) : "";
		}
		else {
			return (parentCtxAttrValue!= '' && parentCtxAttrValue != null) ? getHTMLEncode(eval("JSON.parse(parentCtxAttrValue)."+childAttrName)) : "";
		}
	} catch(e) {
		return "";
	}
};
/**
 * Return the  label after replacing the placeholder
 * @param label
 * @param placeHolderKey
 * @param textToReplace
 * @returns
 */
function getLabelAfterPlaceholderReplacing(label, placeHolderKey, textToReplace, withoutSpan){	
	if(withoutSpan){
		return label.replace(placeHolderKey, textToReplace);
	}
	
	return label.replace(placeHolderKey, "<span>"+textToReplace+"</span>");
};

/**
 * Return the  label after replacing the placeholder
 * @param label
 * @param placeHolderKey
 * @param textToReplace
 * @returns
 */
function getLabelAfterPlaceholderReplacingWithLink(label, placeHolderKey, textToReplace, contentType, contentId, owner, scope){
	if (!isDefined(scope)) {
		scope = "STREAM";
	} 
	
	var link = navigateToContentCode(scope, owner, contentType, contentId);
	
		return label.replace(placeHolderKey, '<a href="#" onclick="'+link+'">'+ getHTMLEncode(textToReplace)+'</a>');
};

/**
 * Return the report column sorting action
 * @param ctx
 * @param colId
 * @param sortType
 * @returns {String}
 */
function getColHeaderSortAction(ctx, colId, sortType){
	var reportId = getCtxValue("reportId", ctx);
	var reportTableId = getCtxValue("reportTableId", ctx).replace("RT", "");
	return " javascript:submitForm('LW_OW_ADD_METRIC_ORDER','"+ colId+"','"+sortType+"','','','','"+reportId+"','"+reportTableId+"'); ";
};
/**
 * Return the context attribute value reading from the ctx string
 * @param ctxString
 * @param childAttrName
 * @returns
 */
function getCtxAttrValueFromCtxString(ctxString, childAttrName){
	try {
		var parentRasterContext = JSON.parse(ctxString);
		var parentCtxAttrValue = getCtxValue(childAttrName, parentRasterContext.ctx);
		return parentCtxAttrValue;
	} catch(e) {
		return "";
	}
};
/**
 * Return the label for the involved users discussion
 * @param ctxString
 * @returns {String}
 */
function getInvolvedUsersLabelFromSpecificInfo(ctxString, showOneUserOnly, withoutHTML){
	try {
		var res = "";
		var parentCtxAttrValue = getCtxAttrValueFromCtxString(ctxString, 'specificInfo');		
		
		if (parentCtxAttrValue!= '' && parentCtxAttrValue != null){
			
			var specificInfo = JSON.parse(parentCtxAttrValue);				
			
			var usersCtx = specificInfo.users;			
			var usersCount = specificInfo.usersCount;
			var authorId = specificInfo.contentPreview.authorId;
			var modifierId = specificInfo.contentPreview.modifierId;
			
			var minUsers = (showOneUserOnly)? 0 : 1;
			if (usersCtx!=null && usersCtx.length >minUsers){
				//res = getLabel('involvedUsersLabel');
				for (var i=0; i < usersCtx.length; i++){
					if (!withoutHTML){
						res = res + '<span>' + usersCtx[i].name + '</span>,&nbsp;';
					}else{
						res = res + usersCtx[i].name + ',&nbsp;';
					}
				}		
			}
			res = res.substring(0,res.lastIndexOf(","));
			
			if (usersCtx!=null && usersCtx!='' && usersCount > 0){
				if (!withoutHTML){
					res = res + ' ' + getLabel('andOther') + ' <span>' +usersCount + '</span>'+ '&nbsp;'+ LANGUAGES.getOriginalLabel('usersLowCase');
				}else{
					res = res + ' ' + getLabel('andOther') + ' ' +usersCount + '&nbsp;'+ LANGUAGES.getOriginalLabel('usersLowCase');
				}
			}			
		}
		return res;
	} catch(e) {
		logToConsole(e.name + ' - ' + e.message);
		return "";
	}
};
/**
 * Return the label for the involved spaces discussion
 * @param ctxString
 * @returns {String}
 */
function getInvolvedSpacesLabelFromSpecificInfo(ctxString, withoutLabel, withoutHTML){
	try {
		var res = "";
		var parentCtxAttrValue = getCtxAttrValueFromCtxString(ctxString, 'specificInfo');		
		
		if (parentCtxAttrValue!= '' && parentCtxAttrValue != null){
			
			var specificInfo = JSON.parse(parentCtxAttrValue);			
		
			var spacesCtx = specificInfo.spaces;			
			if (!spacesCtx)
				spacesCtx = specificInfo.contentPreview.spaces;
				
			if (spacesCtx!=null && spacesCtx.length >0){
				if (!withoutLabel)
					res = getLabel('inLowCase');
				
				for (var i=0; i < spacesCtx.length; i++){
					if (!withoutHTML){
						res = res + '&nbsp;<span>' + getHTMLEncode(spacesCtx[i].displayName) + '</span>,&nbsp;';
					}else{
						res = res + '&nbsp;' + getHTMLEncode(spacesCtx[i].displayName) + ',&nbsp;';
					}
				}
			}
			res = res.substring(0,res.lastIndexOf(","));
			
			var spacesCount = specificInfo.spacesCount;
			if (spacesCtx!=null && spacesCtx!='' && spacesCount > 0){
				if (!withoutHTML){
					res = res + ' ' + getLabel('andOther') + ' <span>' +spacesCount + '</span>'+ '&nbsp;'+ LANGUAGES.getOriginalLabel('spacesLowCase');
				}else{
					res = res + ' ' + getLabel('andOther') + ' ' +spacesCount + '&nbsp;'+ LANGUAGES.getOriginalLabel('spacesLowCase');
				}
			}	
		}
		return res;
	} catch(e) {
		logToConsole(e.name + ' - ' + e.message);
		return "";
	}
};

/**
 * Redirect to the selected space 
 * @param owner
 * @param dshID
 * @param contentId
 * @param contentType
 * "1010": SOCIAL_AREA_OF_GROUP ; "1005": SOCIAL_AREA_NORMAL, "1011": SOCIAL_INITIATIVE
 */
function manageContentForwardSpace(owner, mshPageID, contentType, descContentType){
	closeAllTips();
	closeAllRootJqDialog();
	return rootPage.centrale.location.href='../dsh/dshMain.jsp.cas?csMLO=' + owner + '&dshID='+mshPageID+'&socialPageType='+descContentType+'&contentType='+contentType;
};

/**
 * 
 * @param value
 * @param length
 * @returns
 */
function getTruncString(value, length) {
	if (value.length > length){
		return value.substring(0,length) + "...";
	} else {
		return value;
	}
}

/**
 * Return the context attribute value reading from the ctx string
 * @param ctxString
 * @param childAttrName
 * @returns
 */
function getCtxAttrValueFromContentPreview(ctxString, childAttrName){
	try {
		var res = "";
		
		var parentCtxAttrValue = getCtxAttrValueFromCtxString(ctxString, 'contentPreview');				
		
		if (parentCtxAttrValue!= '' && parentCtxAttrValue != null){			
			var contentPreview = JSON.parse(parentCtxAttrValue);
			var childAttrValue = eval("contentPreview."+childAttrName);	
			if (childAttrValue!=null){
				res = childAttrValue;
			}				
		}
		
		return res;
		
	} catch(e) {
		logToConsole(e.name + ' - ' + e.message);
		return "";
	}
};
/**
 * Return the space id from creation wizard
 * @param ctx
 * @returns
 */
function getSpaceIDFromWizard(ctx){
	var newSpaceID = getPageParamValue('NEW_AREA_ID', getCtxValue('pageParams', ctx));
	var editingSpaceID = getPageParamValue('EDITING_AREA_ID', getCtxValue('pageParams', ctx));
	return (editingSpaceID != "") ? editingSpaceID  : newSpaceID;
};
/**
 * Return the annotation reference object infos
 */
function getAnnotationReferenceObjInfo(ctxString){
	var refObjKeyDesc = getCtxAttrValueFromSpecificInfo(ctxString, 'refObjKeyDesc');
	if (refObjKeyDesc!= '' && refObjKeyDesc != null){		
		var title = JSON.parse(refObjKeyDesc).title;
		var objTypeDesc = JSON.parse(refObjKeyDesc).objectTitle;
		return  objTypeDesc + ': ' + getHTMLEncode(title)+'';
	}
	return "";
};
/**
 * Return the inbox join discussion description
 * @param eventUserID
 * @param eventUserFullName
 * @param joinUserID
 * @param joinUserFullName
 * @param joinState
 * @param spaceType
 * @param spaceID
 * @param spaceName
 */
function getInboxJoinDiscussionDescription(eventUserID, eventUserFullName, joinUserID, joinUserFullName, joinState, spaceType, spaceID, spaceName){
	var joinDiscussionLabel = getLabel('JOIN_'+joinState+'_'+spaceType);
	var eventUserLink = "<a "+anchorUser(eventUserID)+"><span>"+eventUserFullName+"</span></a>";
	var joinUserLink = "<a "+anchorUser(joinUserID)+"><span>" + getHTMLEncode(joinUserFullName) + "</span></a>";
	var spaceLink = "<a href='#' onclick=\""+navigateToContentCode('STREAM', 'SOCIALM', spaceType, spaceID, '')+"\"><span>" + getHTMLEncode(spaceName) + "</span></a>";
	var tmpReplaceEventUser = replaceAll(joinDiscussionLabel, "%1", eventUserLink);
	var tmpReplaceJoinUser = replaceAll(tmpReplaceEventUser, "%2", joinUserLink);
	var tmpReplaceSpace = replaceAll(tmpReplaceJoinUser, "%3", spaceLink);
	return tmpReplaceSpace;
};
function initWelcome(){
	//return JSON.stringify(this.data.userPreferredContents);
	//return this.data.userPreferredContents;
	welcomeData = this.data.userPreferredContents.contents;
	if( this.data.models && this.data.models.list ) {
		welcomeDataModels = this.data.models.list;
	}
	if( this.data.userSpaceGroups && this.data.userSpaceGroups.spaceGroups ) {
		welcomeDataSpaces = this.data.userSpaceGroups.spaceGroups;
	}
};

function getEnvVar( key ){
	return(  ENV_VARS.getValue( key ) );
};

// Restituisce un array di id, a partire da un array di socialSpace
function getExcludedIds(beans) {
	var result = new Array();
	
	for (var bean in beans) {
		result.push(beans[bean].id)
	}
	
	return result;
}

/**
 * Si occupa di trasformare un testo libero (es.www.dcy.it) in un link (tagCollector)
 * Author : Pagano Francesco
 */
function toLinkingText(text){

    var leftDistance = screen.width/2 - 700/2 -150;
    var topDistance = screen.height/2 - 450/2;
	
    //Potrebbe arrivare del testo che contiene tag Html, in merito alla vecchia gestione 
    //in cui non venivano skippate le chiavi lato server
    var textEncoded = text;
    
    option = 	"\'location=no,menubar=no,toolbar=no,dependent=yes,minimizable=no,"+
				" modal=yes,alwaysRaised=yes,resizable=yes,scrollbars=yes,width=900,height=600,"+
				" top="+topDistance+",left="+leftDistance+"\'";
    
    
    textReplace = textEncoded.replace(/(https\:\/\/|http:\/\/|ftp:\/\/)([www\.]?)([^\s|<|>|"|']+)/gi,'<a class="linking" onclick="openLinkWindow(\'$1$2$3\', null, '+option+');">$1$2$3</a>');
    
    if(textReplace != textEncoded){
    	return textReplace;
    }
    
    textReplace = textEncoded.replace(/([^https\:\/\/]|[^http:\/\/]|[^ftp:\/\/]|^)(www)\.([^\s|<|>|"|']+)/gi,'$1<a class="linking" onclick="openLinkWindow(\'http://$2.$3\', null, '+option+');">$2.$3</a>');
    
    if(textReplace != textEncoded){
    	return textReplace;
    }
    
    textReplace = textEncoded.replace(/([^https\:\/\/]|[^http:\/\/]|[^ftp:\/\/]|^)(ftp)\.([^\s|<|>|"|']+)/gi,'$1<a class="linking" onclick="openLinkWindow(\'ftp://$2.$3\', null, '+option+');">$2.$3</a>');
    
    if(textReplace != textEncoded){
    	return textReplace;
    }
    
    //Se il match non ha restituito alcun risultato
    //ritorno la stringa passata originariamente
    return text;
}
 
/**
 * Constrisce un oggetto RenderedData per la renderizzazione di template da javascript
 * 
 * @param view: template
 * @param dataNode: oggetto con i data del template
 * @returns: oggetto RenderedData
 */
function buildNewRenderedData(view, dataNode) {
	var dataObj = {};
	dataObj.view = view;
	dataObj.data = dataNode;
	return dataObj;
}

/**
 * Costruisce un data provider error da una eccezione javascript per un DshElement e setta l'elemento in errore
 * 
 * @param elementId
 * @param $object
 * @param errorCode
 * @param exception
 * @param view
 */
function dataProviderErrorDshElement(elementId, $object, errorCode, exception, view) {
	dataProviderErrorException($object, errorCode, exception, view);
	DSH_PAGE.setDshElementAsInError(elementId);
}

/**
 * Costruisce un data provider error da una eccezione javascript
 * 
 * @param $object: jquery object con il div dove renderizzare l'errore
 * @param view: template di errore
 * @param errorCode: codice di errore
 * @param exception: oggetto Error di javascript
 */
function dataProviderErrorException($object, errorCode, exception, view) {
	dataProviderError($object, errorCode, getStackTrace(exception), view);
}

/**
 * Costruisce un data provider error da javascript senza passare per la gestione server  
 * 
 * @param $object: jquery object con il div dove renderizzare l'errore
 * @param view: template di errore
 * @param errorCode: codice di errore
 * @param details: dettagli se specificati
 */
function dataProviderError($object, errorCode, details, view) {
	if (view == null) 
		view = "defaultError_BaseTMPL";

	var dataNode = new Object();
	dataNode.error = new Object();
	dataNode.error.errorCode = errorCode;
	dataNode.error.errorMsg = getLabel(errorCode);
	if (details != null)
		dataNode.error.errorDetails = details;
	var renderedData = buildNewRenderedData(view, dataNode);
	decorateContentToSelector(renderedData, $object);
}

/**
 * Si occupa di visualizzare il menu dell'Header del documento/blog
 */
var showMenuItemSocialContentTooltip = function (node, objectId, contentType, versionNumber, canModify, isOldRevision, contentUID, instanceReference, buttonEntryCount) {	
	
	var ctxForActions 	= buildNewCtxObject();
	addDataToCtx(ctxForActions, "objectID", 	objectId);
	addDataToCtx(ctxForActions, "canModify", 	canModify);
	addDataToCtx(ctxForActions, "isOldRevision",isOldRevision);
	addDataToCtx(ctxForActions, "contentTypes", contentType);
	addDataToCtx(ctxForActions, "versionNumber",versionNumber);
	addDataToCtx(ctxForActions, "contentUID", 	contentUID);
	
	// genera il ctx container in formato raster
	var rasterCtx = getRasterCtxContainer(ctxForActions);
	var myOptions = {
			my: 'top center',
			at: 'bottom center',
			viewport: $(window),
			position: {
				effect: false,
				viewport: $(window),
				adjust: {
					method: 'flip'
				}
			},
			style: {
				tip: true,
				widget : true,
				classes: 'de_qtip',
			},
			hide: {
				event: 'unfocus', 
				inactive: 5000
			},
			show: {
				event: 'click'
			}
		};
	createJQTooltipWithDataProvider(node, '', "socialContentToolbar", myOptions, rasterCtx);
};

/**
 * Stampa la funzione per per il menù del documento/blog
 * @param ctx
 * @param view
 * @returns {String}
 */
function menuItemSocialContentTooltipFactory(ctx, view) {
	return "showMenuItemSocialContentTooltip(this, '" + view.socialContent.id + "', " + view.socialContent.contentType + ", " + view.socialContent.socialContentVersion.versionNumber + ", " + view.socialContent.canModify + ", " + view.socialContent.isOldRevision + ", '" + getCtxValue("contentUID", ctx) + "', '" + getCtxValue("instanceReference", ctx) + "','" + getCtxValue("buttonEntryCount", ctx) + "');";
}

/**
 * Restituisce una stringa in lower case
 * @param string
 * @returns {String}
 */
function toLowerCase(string) {
	return string.toLowerCase();
}

function manageErrorDPR_EDTPR000(jsonContent, dpParams) {
	var decorateFunct = dpParams.errorCallback ? dpParams.errorCallback : decorateContentToSelector;
	decorateFunct(jsonContent, dpParams.selector, dpParams.controllerUid, dpParams.contextIn);
}

/**
 * Restituisce la descrizione della timezone abbreviata 
 * @param desc
 */
function getShortTimezoneDesc(optionDescription){
	if (optionDescription.length > 72 ){
		optionDescription = optionDescription.substr(0, 72);
		optionDescription = optionDescription.substr(0, optionDescription.lastIndexOf(",")) + " ...";
	}
	return optionDescription;
}
